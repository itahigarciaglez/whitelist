#include "user.h"



vector<vector<Purchase>> User::getHistory() const
{
    return history;
}

void User::setHistory(const vector<vector<Purchase>> &value)
{
    history = value;
}


vector<QVector<QString> > User::getDates() const
{
    return dates;
}

void User::setDates(const vector<QVector<QString> > &value)
{
    dates = value;
}

User::User() {}

void User::setUsername(const QString & u)
{
    username=u;
}

void User::setEmail(const QString & e)
{
    email=e;
}

void User::setPassword(const QString & p)
{
    password=p;
}

QString User::getUsername() const
{
    return username;
}

QString User::getEmail() const
{
    return email;
}

QString User::getPassword() const
{
    return password;
}
