#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->viewSW->setCurrentIndex(0);

    openFileAction = new QAction("&Open database", this);
    connect(openFileAction, SIGNAL(triggered()), this, SLOT(openFile()));
    ui->menubar->addMenu("&File")->addAction(openFileAction);
}

MainWindow::~MainWindow()
{
    if(cart.size()>0){
        vector<vector<Purchase>> aux=currentUser.getHistory();
        vector<QVector<QString>> aux2;//todas las llaves
        QVector<QString> aKey;
        aux.push_back(cart);//añade el carrito actual

        QString date=QDateTime::currentDateTime().toString("dd/MM/yyyy' 'hh:mm:ss");
        aux2=currentUser.getDates();//obtiene todas las llaves
        aKey.push_back(date);
        aux2.push_back(aKey);

        currentUser.setDates(aux2);
        currentUser.setHistory(aux);
    }
    saveDB();
    delete ui;
}

void MainWindow::enableLoginPB()
{
    if(ui->usernameLE->text().length()>0 &&
       ui->passwordLE->text().length()>0){
        ui->loginPB->setEnabled(true);
    }
    else{
        ui->loginPB->setEnabled(false);
    }
}

void MainWindow::enableSignUpPB()
{
    if (ui->newUserLE->text().length()>0 &&
            ui->emailLE->text().length()>0 &&
            ui->newPasswordLE->text().length()>0)
    {
        ui->signInPB->setEnabled(true);
    }
    else{
        ui->signInPB->setEnabled(false);
    }
}

void MainWindow::showAllProducts()
{
    int row(0),column(0);
    for(size_t i(0);i < products.size();i++){
        if(nameFilter(products[i].getName().toUpper(), ui->searchLE->text().toUpper())){
           myProduct= new ProductWidget();
           myProduct->showProducts(products[i]);
           connect(myProduct, SIGNAL(addItem(QString,int)),this, SLOT(addToCart(QString,int)));
           ui->layoutSA->addWidget(myProduct,row,column,Qt::AlignHCenter);

           if(column<2){
               column++;
           }
           else{
               column=0;
               row++;
           }
        }
    }
}

void MainWindow::showByDepartment(const QString& dept)
{
    int row(0),column(0);
    for(size_t i(0);i<products.size();i++){
        if(products[i].getId().at(0)==dept){//si encajan con el filtro
           if(nameFilter(products[i].getName().toUpper(), ui->searchLE->text().toUpper())){
               myProduct= new ProductWidget();
               myProduct->showProducts(products[i]);
               connect(myProduct, SIGNAL(addItem(QString,int)),this, SLOT(addToCart(QString,int)));
               ui->layoutSA->addWidget(myProduct,row,column,Qt::AlignHCenter);

               if(column<2){
                   column++;
               }
               else{
                   column=0;
                   row++;
               }
           }
        }
    }
}

bool MainWindow::nameFilter(const QString & proName, const QString & filter)
{
    if(filter.isEmpty()){
        return true;//si no hay nada en el filtro, regresa verdadero
    }

    int j, isValid=0;

    //como si es verdadero regresaría, no es necesario poner else
    for(int i(0); i<proName.length();i++){
       j=0;
        while(j<filter.length()){
            if(j+i<proName.length() && filter[j]==proName[j+i]){//j+i<proname.length para evitar que se busque un caracter en una posición inválida
                //se pone j+i ya que j es la posición del filtro de búsqueda en el que se encuentre e i es la posición del nombre del producto en el que se encuentra
                isValid++;
            }
            j++;
        }
        if (isValid==filter.length()){//si coincide con el filtro
            return true; //regresa verdadero
        }
        isValid=0;//si no coincide con el filtro, se reinicia el contador
    }
    return false;//si nunca coincide con el filtro, retorna falso
}

void MainWindow::sortProducts(const int& opc)
{
    float factor(1.0/2.0);
    int dif=static_cast<int>(products.size())*factor,i,j;
    Product aux;

    switch (opc) {
        case -1://desacomodar
        products=ProductAux;
            break;

        case 0://Ascendente
        while(dif>0){
            i=dif;
            while(i<=static_cast<int>(products.size()-1)){
                j=i;
                while(j>=dif and (products[j-dif].getPrice()>products[j].getPrice())){
                    aux=products[j-dif];
                    products[j-dif]=products[j];
                    products[j]=aux;//se intercambian los productos de posición

                    j-=dif;
                }
                i++;
            }
            dif*=factor;
        }
            break;
        case 1://Descendente
            while(dif>0){
              i=dif;
              while(i<=static_cast<int>(products.size()-1)){
                 j=i;
                 while(j>=dif and (products[j-dif].getPrice()<products[j].getPrice())){
                       aux=products[j-dif];
                       products[j-dif]=products[j];
                       products[j]=aux;//se intercambian los productos de posición

                       j-=dif;
                 }
                 i++;
               }
            dif*=factor;
            }
            break;
    }
}

void MainWindow::validateUser()
{
    QMessageBox message;
    vector<User>::iterator it;
    int i(0);
    QString user=ui->usernameLE->text();
    QString password=ui->passwordLE->text();

    it= find_if(users.begin(), users.end(), [&user, &password](User u)->bool
    {
       return u.getUsername() == user && u.getPassword() == password;
    }
    );

    if (it==users.end()){
        message.setText("Invalid credentials");
        message.setIcon(QMessageBox::Warning);

        message.exec();
    }
    else{
        while(users[i].getUsername()!=user){
            i++;
        }
        currentUser=users[i];
        message.setText("Welcome to LERMA " + user);
        ui->viewSW->setCurrentIndex(1);
        notDept=false;
        ui->sortCB->setCurrentIndex(-1);
        notDept=true;
        showAllProducts();
        message.exec();

    }
}

bool MainWindow::isValidEmail()
{
    QString exp("[(0-9)*(a-z)+(_)*(-)*(.)*(?)*(1+9)*(_)*(-)*(.)*(?)*]+[@{1}][a-z]+[.][a-z]+");

    QRegularExpression valid(exp);
    return valid.match(ui->emailLE->text()).hasMatch();

    //regresa el si lo que se encuentre en emailLE va de acuerdo con la expresión regular
}

bool MainWindow::isUnique()
{
    vector<User>::iterator it;
    QString user=ui->newUserLE->text();
    QString email=ui->emailLE->text();

    it= find_if(users.begin(), users.end(), [&user, &email](User u)->bool
    {
       return u.getUsername() == user || u.getEmail() == email; //si el nombre de usuario o la contraseña concuerdan
    }
    );

    if (it==users.end()){ //si no encuentra algún usuario parecido
        return true;
    }
    else{ //Si se encuentra al usuario
        return false;
    }
}

void MainWindow::saveDB()
{
    QJsonObject jsonObj;
    QJsonDocument jsonDoc;
    savePurchases();

    jsonObj["products"]=prArray;
    jsonDoc=QJsonDocument(jsonObj);
    jsonObj["users"]=dbArray;
    jsonDoc=QJsonDocument(jsonObj);

    dbFile.open(QIODevice::WriteOnly);
    dbFile.write(jsonDoc.toJson());
    dbFile.close();
}

void MainWindow::loadDB()
{
    QJsonObject jsonObj;
    QJsonDocument jsonDoc;

    QByteArray data;

    dbFile.open(QIODevice::ReadOnly);
    data=dbFile.readAll();
    dbFile.close();


    jsonDoc=QJsonDocument::fromJson(data);
    jsonObj=jsonDoc.object();
    prArray=jsonObj["products"].toArray();

    for(int i=0;i<prArray.size();i++){
        Product p;
        QJsonObject obj=prArray[i].toObject();
        p.setId(obj["id"].toString());
        p.setName(obj["name"].toString());
        p.setPrice(obj["price"].toDouble());
        products.push_back(p);
    }//Agrega los productos

    dbArray=jsonObj["users"].toArray();
    ProductAux=products;

    for(int i=0;i<dbArray.size();i++){
        User u;
        QJsonArray aux2;
        vector<vector<Purchase>> auxVector;//para agregarle todos los carritos al usuario
        vector<QVector<QString>>allKeys;
        QJsonObject obj=dbArray[i].toObject();
        u.setUsername(obj["name"].toString());
        u.setEmail(obj["email"].toString());
        u.setPassword(obj["password"].toString());

        QJsonArray aux1=obj["purchase"].toArray();//vector purchase

        for(int j=0;j<aux1.size();j++){
            QJsonObject newCar=aux1[j].toObject();//convierte en objeto lo que esté en el vector de compras en la posición j
            QVector<QString> cartKey= newCar.keys().toVector();//llave de carrito
            vector<Purchase> productsPurchased;
            allKeys.push_back(cartKey);
            aux2=newCar[cartKey[0]].toArray();//convierte en array el objeto, 0 porque siempre es el primer elemento

            for(int k(0);k<aux2.size();k++){
                Purchase p;
                QJsonObject newPurchase=aux2[k].toObject();//obtiene el objeto que esté en aux en la posición k
                p.setId(newPurchase["id"].toString());
                p.setQuantity(newPurchase["units"].toInt());

                productsPurchased.push_back(p);//añade el producto al vector

            }
            auxVector.push_back(productsPurchased);//añade el carrito de esa fecha al vector
        }//vector purchase
        u.setDates(allKeys);//pone las fechas de compra de cada carrito en el usuario
        u.setHistory(auxVector);
        users.push_back(u);

    }//Agrega usuarios
    createGraph();

}

QJsonArray MainWindow::saveItems(const User &u, const int &i)
{
    QJsonArray items;
    vector<vector<Purchase>> aux=u.getHistory();
    if(aux[i].size()>0){//si el vector de productos en la posición dada del vector de carritos es mayor a 0
        for(size_t j(0);j<aux[i].size();j++){
            QJsonObject pro;
            pro["id"]=aux[i][j].getId();
            pro["units"]=aux[i][j].getQuantity();
            items.append(pro);//añade el producto al vector de carritos
        }
    }
    return items;//regresa el carrito de esa fecha
}


QJsonArray MainWindow::saveCart(const User& u)
{
    QJsonArray carts;
    vector<QVector<QString>> allDates=u.getDates();
    if(u.getHistory().size()>0){
        for(size_t i(0);i<u.getHistory().size();i++){
            QJsonObject newCart;
            QVector<QString> aKey=allDates[i];
            newCart[aKey[0]]=saveItems(u,i);//el carrito de la posición i será igual al array del método saveItems
            carts.append(newCart);
        }
    }

    return carts;//regresa los carritos
}

void MainWindow::savePurchases()
{
    QJsonArray updatedUser;
    for(size_t i(0);i<users.size();i++){
        QJsonObject u;
        u=dbArray[i].toObject();//obtiene el usuario
        if(users[i].getUsername()==currentUser.getUsername()){
            u["purchase"]=saveCart(currentUser);//agrega el atributo purchase
        }
        else{
            u["purchase"]=saveCart(users[i]);//agrega el atributo purchase
        }
        updatedUser.append(u);//añade u
    }
    dbArray=updatedUser;//actualiza los usuarios para que tengan los carritos cambiados
}

void MainWindow::createGraph()
{
    vector<vector<Purchase>> aux;
    vector<Purchase> pro;

    for(size_t i=0; i<users.size();i++){//para todos los usuarios
        aux=users[i].getHistory();
        for(size_t j=0;j<aux.size();j++){//todos los carritos
            pro=aux[j];//un carrito
            for(size_t k=0; k<pro.size();k++){//todos los productos de un carrtito
                if(k+1<pro.size()){
                    for(size_t l=k+1;l<pro.size();l++){
                        if(!productGraph.isEdge(pro[k].getId().toStdString(),pro[l].getId().toStdString())){
                            productGraph.createEdge(pro[k].getId().toStdString(),pro[l].getId().toStdString(),1);
                        }
                        else{
                            int cost=productGraph.getCost(pro[k].getId().toStdString(),pro[l].getId().toStdString());
                            cost++;
                            productGraph.createEdge(pro[k].getId().toStdString(),pro[l].getId().toStdString(),cost);
                        }
                    }
                }
            }
        }
    }
    productGraph.printData();
}

void MainWindow::on_usernameLE_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1);
    enableLoginPB();
}

void MainWindow::on_passwordLE_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1);
    enableLoginPB();
}

void MainWindow::on_newUserLE_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1);
    enableSignUpPB();
}

void MainWindow::on_emailLE_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1);
    enableSignUpPB();
}

void MainWindow::on_newPasswordLE_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1);
    enableSignUpPB();
}

void MainWindow::on_signInPB_clicked()
{
    QJsonObject jsonObj;
    QMessageBox message;
    QJsonArray aux;
    User u;

    if(isValidEmail() && isUnique()){
        u.setUsername(ui->newUserLE->text());
        u.setEmail(ui->emailLE->text());
        u.setPassword(ui->newPasswordLE->text());

        users.push_back(u);

        message.setText("New user created");
        message.exec();

        ui->newUserLE->clear();
        ui->emailLE->clear();
        ui->newPasswordLE->clear();
        jsonObj["name"]=u.getUsername();
        jsonObj["email"]=u.getEmail();
        jsonObj["password"]=u.getPassword();

        jsonObj["purchase"]=aux;
        dbArray.append(jsonObj);

    }

    else{
        message.setText("Unable to create user");
        message.exec();
    }
}

void MainWindow::on_loginPB_clicked()
{
    validateUser();
    ui->usernameLE->clear();
    ui->passwordLE->clear();
}

void MainWindow::openFile()
{
    QString name;

    name=QFileDialog::getOpenFileName(this, "Select database","","JSON files(*.json)");
    if (name.length()>0){
        dbFile.setFileName(name);
        ui->loginGB->setEnabled(true);
        ui->signInGB->setEnabled(true);
        loadDB();
    }
}

void MainWindow::on_filterCB_currentIndexChanged(int index)
{
    while (!ui->layoutSA->isEmpty()){
            QWidget *w=ui->layoutSA->takeAt(0)->widget();
            delete w;
    }


    notDept=false;
    ui->sortCB->setCurrentIndex(-1);
    ui->searchLE->setText("");

    sortProducts(-1);//reinicia el acomodo

    switch (index){
    case 0: //todos los departamentos
        showAllProducts();
        break;
    case 1://Alimentos y Bebidas
        showByDepartment("A");
        break;
    case 2://Libros
        showByDepartment("L");
        break;
    case 3://Electrónicos
        showByDepartment("E");
        break;
    case 4://Hogar Y Cocina
        showByDepartment("H");
        break;
    case 5://Deporte y Aire Libre
        showByDepartment("D");
        break;
    }
    notDept=true;
}

void MainWindow::on_sortCB_currentIndexChanged(int index)
{
    if(notDept){
        while (!ui->layoutSA->isEmpty()){
                QWidget *w=ui->layoutSA->takeAt(0)->widget();
                delete w;
        }

        sortProducts(index);//Se acomodan los productos

        switch(ui->filterCB->currentIndex()){
            case 0: //todos los departamentos
                showAllProducts();
            break;
            case 1://Alimentos y Bebidas
                showByDepartment("A");
            break;
            case 2://Libros
                showByDepartment("L");
            break;
            case 3://Electrónicos
                showByDepartment("E");
            break;
            case 4://Hogar Y Cocina
                showByDepartment("H");
            break;
            case 5://Deporte y Aire Libre
                showByDepartment("D");
            break;
        }
    }
}

void MainWindow::on_searchLE_textChanged()
{
    if(notDept){
        while (!ui->layoutSA->isEmpty()){
                QWidget *w=ui->layoutSA->takeAt(0)->widget();
                delete w;
        }

        switch (ui->filterCB->currentIndex()){
        case 0: //todos los departamentos
            showAllProducts();
            break;
        case 1://Alimentos y Bebidas
            showByDepartment("A");
            break;
        case 2://Libros
            showByDepartment("L");
            break;
        case 3://Electrónicos
            showByDepartment("E");
            break;
        case 4://Hogar Y Cocina
            showByDepartment("H");
            break;
        case 5://Deporte y Aire Libre
            showByDepartment("D");
            break;
        }
    }
}

void MainWindow::addToCart(QString id, int unit)
{
    Purchase newPurchase;
    newPurchase.setId(id);
    newPurchase.setQuantity(unit);

    cart.push_back(newPurchase);//añade los datos del producto al carrito
}
