#ifndef PRODUCTWIDGET_H
#define PRODUCTWIDGET_H

#include <QWidget>
#include <vector>
#include <QGridLayout>
#include <QLabel>
#include <string>
#include <QPixmap>
#include <QImage>
#include "product.h"

using namespace std;

namespace Ui {
class ProductWidget;
}

class ProductWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ProductWidget(QWidget *parent = 0);
    ~ProductWidget();

    void showProducts(const Product&);

private slots:
    void on_addPB_clicked();

    void on_spinBox_valueChanged(int arg1);
signals:
    void addItem(QString id, int amount);
private:
    Ui::ProductWidget *ui;
    QString productId;

};

#endif // PRODUCTWIDGET_H
