#ifndef PURCHASE_H
#define PURCHASE_H
#include <vector>
#include <QString>
#include "product.h"

using namespace::std;
class Purchase
{
private:
    QString id;
    int quantity;
public:
    Purchase();
    QString getId() const;
    void setId(const QString &value);
    int getQuantity() const;
    void setQuantity(int value);
};

#endif // PURCHASE_H
