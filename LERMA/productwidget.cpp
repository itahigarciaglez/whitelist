#include "productwidget.h"
#include "ui_productwidget.h"


ProductWidget::ProductWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProductWidget)
{
    ui->setupUi(this);
}

ProductWidget::~ProductWidget()
{
    delete ui;
}

void ProductWidget::showProducts(const Product& products)
{
    int row(0);

    QLabel* img=new QLabel();//nueva etiqueta para la imagen
            QString myStr=products.getName()+'\n'+QString::number(products.getPrice(),'f',2);
            QLabel* info=new QLabel();

            img->setScaledContents(true);//La etiqueta tiene contenido escalado
            img->setFixedHeight(250);
            img->setFixedWidth(250);
            info->setText(myStr);
            info->setWordWrap(true);
            info->setAlignment(Qt::AlignHCenter);

            QPixmap imgPM(":/img/imgs/"+products.getId()+".jpg");//La imagen es aquella que tenga como nombre el id del producto
            img->setPixmap(imgPM.scaled(img->width(),img->height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));//Añade la imagen a la etiqueta
            ui->labelsG->addWidget(img,row,0); //añade la imagen en la columna y fila actual

            row++;
            ui->labelsG->addWidget(info,row,0);//agrega la etiqueta de información del producto

            productId=products.getId();
}


void ProductWidget::on_addPB_clicked()
{
    emit addItem(productId, ui->spinBox->value());
    ui->spinBox->setValue(0);
}

void ProductWidget::on_spinBox_valueChanged(int arg1)
{
    if (arg1>0){
        ui->addPB->setEnabled(true);//habilita el botón de añadir
    }
    else{
        ui->addPB->setEnabled(false);
    }
}
