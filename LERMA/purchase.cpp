#include "purchase.h"


QString Purchase::getId() const
{
    return id;
}

void Purchase::setId(const QString &value)
{
    id = value;
}

int Purchase::getQuantity() const
{
    return quantity;
}

void Purchase::setQuantity(int value)
{
    quantity = value;
}

Purchase::Purchase()
{

}
