#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <vector>
#include "user.h"
#include "product.h"
#include "productwidget.h"
#include "purchase.h"
#include <QRegularExpression>
#include <QAction>
#include <QFile>
#include <QFileDialog>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDateTime>
#include "graph.h"

using namespace std;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class ProductWidget;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    virtual ~MainWindow();

private slots:
    void on_usernameLE_textChanged(const QString &arg1);

    void on_passwordLE_textChanged(const QString &arg1);

    void on_newUserLE_textChanged(const QString &arg1);

    void on_emailLE_textChanged(const QString &arg1);

    void on_newPasswordLE_textChanged(const QString &arg1);

    void on_signInPB_clicked();

    void on_loginPB_clicked();

    void openFile();

    void on_filterCB_currentIndexChanged(int index);

    void on_sortCB_currentIndexChanged(int index);

    void on_searchLE_textChanged();

    void addToCart(QString,int);

private:
    Ui::MainWindow *ui;
    ProductWidget *myProduct;
    int currentIndex=0;//indice del carrito actual

    QAction* openFileAction;
    QFile dbFile;
    QJsonArray dbArray;
    QJsonArray prArray;
    bool notDept=true;//No fue cambio de departamento

    Graph<string> productGraph;
    vector<User> users;
    User currentUser;
    vector<Purchase> cart;//carrito actual
    vector<Product> products;
    vector<Product> ProductAux;

    void enableLoginPB();
    void enableSignUpPB();

    void showAllProducts();
    void showByDepartment(const QString&);

    bool nameFilter(const QString&, const QString&);
    void sortProducts(const int&);

    void validateUser();
    bool isValidEmail();
    bool isUnique();
    void saveDB();
    void loadDB();
    QJsonArray saveItems(const User&, const int&);
    QJsonArray saveCart(const User&);
    void savePurchases();
    void createGraph();
};
#endif // MAINWINDOW_H
