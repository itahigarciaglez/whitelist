#ifndef USER_H
#define USER_H
#include <QString>
#include <vector>
#include <QVector>
#include "purchase.h"

using namespace::std;
class User{
private:
    QString username;
    QString email;
    QString password;
    vector<QVector<QString>> dates;
    vector<vector<Purchase>> history;
public:
    User();

    void setUsername(const QString&);
    void setEmail(const QString&);
    void setPassword(const QString&);

    QString getUsername() const;
    QString getEmail() const;
    QString getPassword() const;
    vector<vector<Purchase> > getHistory() const;
    void setHistory(const vector<vector<Purchase> > &value);

    vector<QVector<QString> > getDates() const;
    void setDates(const vector<QVector<QString> > &value);
};

#endif // USER_H
