#include "product.h"

QString Product::getName() const
{
    return name;
}

void Product::setName(const QString &value)
{
    name = value;
}

QString Product::getId() const
{
    return id;
}

void Product::setId(const QString &value)
{
    id = value;
}

double Product::getPrice() const
{
    return price;
}

void Product::setPrice(const double &value)
{
    price = value;
}

Product::Product(){}
