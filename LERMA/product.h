#ifndef PRODUCT_H
#define PRODUCT_H
#include <QString>

class Product
{
private:
    QString name;
    QString id;
    double price;
public:
    Product();
    QString getName() const;
    void setName(const QString &value);
    QString getId() const;
    void setId(const QString &value);
    double getPrice() const;
    void setPrice(const double &value);
};

#endif // PRODUCT_H
